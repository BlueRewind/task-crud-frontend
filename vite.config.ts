import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import { resolve } from 'path';
/// <reference types="vitest" />

export default defineConfig({
	plugins: [react()],
	build: {
		outDir: 'build',
	},
	test: {
		global: true,
		environment: 'jsdom',
	},
});
