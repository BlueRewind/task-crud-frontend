`# Task Management App - Frontend

This is the frontend implementation of the Task Management Application, a simple CRUD app for managing tasks. The frontend is built using Vite, React, TypeScript, and Material-UI (MUI) components.

## Prerequisites

Before running the application, make sure you have the following installed:

-   [Node.js](https://nodejs.org/) (v14.0.0 or later)
-   [npm](https://www.npmjs.com/) or [yarn](https://yarnpkg.com/)

## Getting Started

1. Clone the repository:

```bash
git clone git@gitlab.com:BlueRewind/task-crud-frontend.git
cd task-crud-frontend
```

1.  Install dependencies:

```bash
npm install
# or
yarn
```

1.  Run the application:

```bash
npm run dev
# or
yarn dev
```

This will start the development server, and you can view the app at `http://localhost:5173` in your browser.

## Features

-   **Create Task**: Add new tasks with a name, status, priority, and due date.
-   **Read Task**: View a list of existing tasks with details.
-   **Update Task**: Modify task details such as name, status, priority, and due date.
-   **Delete Task**: Remove tasks from the list.

## Tech Stack

-   **Vite**: Fast frontend build tool.
-   **React**: JavaScript library for building user interfaces.
-   **TypeScript**: Adds static typing to JavaScript.
-   **Material-UI (MUI)**: React UI framework for designing consistent and modern interfaces.

## Project Structure

-   `src/`: Contains the application source code.
    -   `components/`: Reusable React components.
    -   `pages/`: Main application pages.
    -   `utils/`: API services and utility functions.
    -   `App.tsx`: Main application component.
    -   `index.tsx`: Entry point of the application.
-   `public/`: Static assets and HTML template.

## API Integration

Make sure the backend server (API) is running.
