# building the app
FROM node:lts-buster-slim AS builder
WORKDIR /app
COPY package*.json ./
RUN npm install --include=dev
COPY . .
RUN npm run build

# nginx stuff
FROM nginx:latest
COPY --from=builder /app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 5173:5173

CMD ["nginx", "-g", "daemon off;"]
