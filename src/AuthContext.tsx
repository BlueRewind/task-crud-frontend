import * as React from 'react';
import { createContext, useContext, useState, useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import axios from 'axios';

const AuthContext = createContext('' as any);

const AuthProvider = ({ children }: { children: React.ReactNode }) => {
	const [isAuthenticated, setAuthenticated] = useState(false);
	const [jwt, setJwt] = useState<null | string>(localStorage.getItem('jwt'));
	const axiosConfig = {
		headers: {
			'Access-Control-Allow-Origin': '*',
		},
	};

	const navigate = useNavigate();
	const location = useLocation();

	useEffect(() => {
		const storedJwt = localStorage.getItem('jwt');
		if (storedJwt !== null) {
			setJwt(storedJwt);
			setAuthenticated(true);
			navigate(location.pathname);
		} else {
			setAuthenticated(false);
			navigate('/');
		}
	}, [isAuthenticated]);

	const login = async (username: string, password: string) => {
		const requestBody = {
			username: username,
			password: password,
		};
		const response = await axios.post(
			'/login',
			requestBody,
			axiosConfig,
		);
		setJwt(response.data);
		setAuthenticated(true);
		localStorage.setItem('jwt', response.data);
		navigate("/tasks");
	};

	const logout = async () => {
		localStorage.removeItem('jwt');

		await axios.get('/logoutUser');
		setAuthenticated(false);
		setJwt(null);
	};

	return (
		<AuthContext.Provider value={{ isAuthenticated, jwt, login, logout }}>
			{children}
		</AuthContext.Provider>
	);
};

const useAuth = () => {
	return useContext(AuthContext);
};

export { AuthProvider, useAuth };
