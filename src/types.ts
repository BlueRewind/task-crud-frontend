export interface Task {
	id: string;
	name: string;
	status: string;
	category: string;
	priority: string;
	dueDate: string;
	creationDate: string;
}

export enum TaskStatus {
	CREATED = 'CREATED',
	FINISHED = 'FINISHED',
	IN_PROGRESS = 'IN_PROGRESS',
	ON_HOLD = 'ON_HOLD',
}

export enum TaskCategory {
	PERSONAL = 'PERSONAL',
	WORK = 'WORK',
	EDUCATION = 'EDUCATION',
	FAMILY = 'FAMILY',
	SOCIAL = 'SOCIAL',
	HEALTH = 'HEALTH',
	FINANCIAL = 'FINANCIAL',
	TRAVEL = 'TRAVEL',
	ENTERTAINMENT = 'ENTERTAINMENT',
}

export interface TaskFormData {
	name: string;
	status: TaskStatus;
	category: TaskCategory;
	priority: number;
	dueDate: string;
}
