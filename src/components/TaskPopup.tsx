import { useForm, SubmitHandler, Controller } from 'react-hook-form';
import {
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogContentText,
	DialogTitle,
	MenuItem,
	Select,
	TextField,
} from '@mui/material';

import { v4 as uuidv4 } from 'uuid';
import axios from 'axios';
import { useAuth } from '../AuthContext';
import { fetchTasks } from '../utils/api';
import { Task, TaskCategory, TaskFormData, TaskStatus } from '../types';

interface TaskPopupProps {
	open: boolean;
	onClose: () => void;
	setTasksCallback: (tasks: Task[]) => void;
}

const TaskPopup = ({
	open,
	onClose,
	setTasksCallback,
}: TaskPopupProps): JSX.Element => {
	const {
		control,
		handleSubmit,
		register,
		formState: { errors },
	} = useForm<TaskFormData>();

	const { jwt } = useAuth();

	const onSubmit: SubmitHandler<TaskFormData> = async (data) => {
		const formattedDate = new Date(data.dueDate).toISOString();
		const requestBody = {
			id: uuidv4(),
			name: data.name,
			status: data.status,
			category: data.category,
			priority: data.priority,
			dueDate: formattedDate,
		};

		const url = '/api/tasks/task';
		try {
			await axios.post(url, requestBody, {
				headers: {
					Accept: 'application/json, text/plain, */*',
					'Access-Control-Allow-Origin': '*',
					Authorization: 'Bearer ' + jwt,
				},
			});
			await fetchTasks(setTasksCallback, jwt);
		} catch (error) {
			console.error('Error fetching data:', error);
		}
		onClose();
	};

	return (
		<Dialog open={open} onClose={onClose}>
			<form onSubmit={handleSubmit(onSubmit)}>
				<DialogTitle>Add Task</DialogTitle>
				<DialogContent>
					<DialogContentText>
						Please fill out the form to add a new task.
					</DialogContentText>

					<TextField
						label="Task Name"
						variant="outlined"
						margin="normal"
						fullWidth
						{...register('name', { required: 'Name is required' })}
					/>

					<Controller
						name="category"
						control={control}
						defaultValue={TaskCategory.EDUCATION}
						render={({ field }) => (
							<Select
								label="Status"
								variant="outlined"
								fullWidth
								{...field}
								{...(errors.status && {
									error: true,
									helperText: 'Category is required',
								})}
							>
								<MenuItem value={TaskCategory.EDUCATION}>
									Education
								</MenuItem>
								<MenuItem value={TaskCategory.ENTERTAINMENT}>
									Entertainment
								</MenuItem>
								<MenuItem value={TaskCategory.FAMILY}>
									Family
								</MenuItem>
								<MenuItem value={TaskCategory.FINANCIAL}>
									Financial
								</MenuItem>
								<MenuItem value={TaskCategory.HEALTH}>
									Health
								</MenuItem>
								<MenuItem value={TaskCategory.PERSONAL}>
									Personal
								</MenuItem>
								<MenuItem value={TaskCategory.SOCIAL}>
									Social
								</MenuItem>
								<MenuItem value={TaskCategory.TRAVEL}>
									Travel
								</MenuItem>
								<MenuItem value={TaskCategory.WORK}>
									Work
								</MenuItem>
							</Select>
						)}
					/>

					<Controller
						name="status"
						control={control}
						defaultValue={TaskStatus.CREATED}
						render={({ field }) => (
							<Select
								label="Status"
								variant="outlined"
								fullWidth
								{...field}
								{...(errors.status && {
									error: true,
									helperText: 'Status is required',
								})}
							>
								<MenuItem value={TaskStatus.CREATED}>
									Created
								</MenuItem>
								<MenuItem value={TaskStatus.IN_PROGRESS}>
									In Progress
								</MenuItem>
								<MenuItem value={TaskStatus.ON_HOLD}>
									On Hold
								</MenuItem>
								<MenuItem value={TaskStatus.FINISHED}>
									Finished
								</MenuItem>
							</Select>
						)}
					/>

					<Controller
						name="priority"
						control={control}
						defaultValue={1}
						render={({ field }) => (
							<Select
								label="Priority"
								variant="outlined"
								fullWidth
								{...field}
								{...(errors.priority && {
									error: true,
									helperText: 'Priority is required',
								})}
							>
								{[1, 2, 3, 4, 5].map((priority) => (
									<MenuItem key={priority} value={priority}>
										{priority}
									</MenuItem>
								))}
							</Select>
						)}
					/>

					<TextField
						label="Due Date"
						variant="outlined"
						margin="normal"
						fullWidth
						type="date"
						InputLabelProps={{ shrink: true }}
						{...register('dueDate', {
							required: 'Due date is required',
						})}
					/>
				</DialogContent>
				<DialogActions>
					<Button onClick={onClose} variant='contained' color="secondary">
						Cancel
					</Button>
					<Button type="submit" variant='contained' color='success'>
						Add Task
					</Button>
				</DialogActions>
			</form>
		</Dialog>
	);
};

export default TaskPopup;
