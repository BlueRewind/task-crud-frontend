import styled from '@emotion/styled';
import { ReactNode } from 'react';
import { Box, Stack, Typography } from '@mui/material';
import MuiAppBar from '@mui/material/AppBar';
import { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import { useTheme } from '@emotion/react';

const StyledBox = styled('div')(({ theme }: any) => ({
	padding: theme.spacing(8),
	backgroundColor: theme.palette.background.paper,
	color: theme.palette.text.primary,
	width: '100%',
	minHeight: '100%',
	margin: '1%',
	boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.1)',
	borderRadius: '8px',
	transition: 'box-shadow 0.3s ease',
	':hover': {
		boxShadow: '0px 0px 20px rgba(0, 0, 0, 0.2)',
	},
	':not(:last-of-type)': {
		marginBottom: theme.spacing(4),
	},
}));

const CustomTypography = styled(Typography)(({ theme }: any) => ({
	flexGrow: 1,
	textAlign: 'center',
	marginRight: '1vw',
	color: theme.palette.text.primary,
}));

export const RightAlignedStack = styled(Stack)(() => ({
	marginLeft: 'auto',
}));

export const AppContainer = styled(Box)`
	width: 100%;
	position: relative;
`;

export interface AppBarProps extends MuiAppBarProps {
	open: boolean;
}

export const AppBar = styled(MuiAppBar, {
	shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }: any) => ({
	transition: theme.transitions.create(['margin', 'width'], {
		easing: theme.transitions.easing.sharp,
		duration: theme.transitions.duration.leavingScreen,
	}),
	...(open && {
		width: `calc(100% - ${250}px)`,
		marginLeft: `${250}px`,
		transition: theme.transitions.create(['margin', 'width'], {
			easing: theme.transitions.easing.easeOut,
			duration: theme.transitions.duration.enteringScreen,
		}),
	}),
}));

export const Main = styled('main', {
	shouldForwardProp: (prop) => prop !== 'open',
})<{
	open?: boolean;
}>(({ theme, open }: any) => ({
	flexGrow: 1,
	padding: theme.spacing(3),
	transition: theme.transitions.create('margin', {
		easing: theme.transitions.easing.sharp,
		duration: theme.transitions.duration.leavingScreen,
	}),
	marginLeft: `-${250}px`,
	...(open && {
		transition: theme.transitions.create('margin', {
			easing: theme.transitions.easing.easeOut,
			duration: theme.transitions.duration.enteringScreen,
		}),
		marginLeft: 0,
	}),
}));

export const DrawerHeader = styled('div')(({ theme }: any) => ({
	display: 'flex',
	alignItems: 'center',
	padding: theme.spacing(0, 1),
	...theme.mixins.toolbar,
	justifyContent: 'flex-end',
}));

interface ThemedComponentProps {
	children: ReactNode;
}

export const BoxComponent = ({ children }: ThemedComponentProps) => {
	const theme = useTheme();
	return <StyledBox theme={theme}>{children}</StyledBox>;
};

interface StyledTypographyProps extends ThemedComponentProps {
	variant: string;
}

export const StyledTypography = ({
	children,
	variant,
}: StyledTypographyProps) => {
	const theme = useTheme();
	return (
		<CustomTypography variant={variant} theme={theme}>
			{children}
		</CustomTypography>
	);
};
