import {
	MaterialReactTable,
	useMaterialReactTable,
	type MRT_ColumnDef,
	MRT_GlobalFilterTextField,
	MRT_ToggleFiltersButton,
} from 'material-react-table';

import { useEffect, useMemo, useState } from 'react';
import { useAuth } from '../AuthContext';
import { Box, Button, MenuItem, lighten } from '@mui/material';
import { deleteTaskFromDB } from '../utils/databaseUtils';
import { deleteAllTasks, fetchTasks } from '../utils/api';
import { Task } from '../types';

interface TaskTableProps {
	data: Task[];
	setTaskCallback: (updatedTasks: Task[]) => void;
}

export const TasksTable = ({
	data,
	setTaskCallback,
}: TaskTableProps): JSX.Element => {
	const columns = useMemo<MRT_ColumnDef<Task>[]>(
		() => [
			{
				accessorKey: 'id',
				id: 'id',
				header: 'Task ID',
				size: 200,
			},
			{
				accessorKey: 'name',
				id: 'name',
				header: 'Name',
				size: 200,
			},
			{
				accessorKey: 'status',
				enableClickToCopy: true,
				filterVariant: 'autocomplete',
				header: 'Status',
				size: 200,
			},
			{
				accessorKey: 'category',
				enableClickToCopy: true,
				filterVariant: 'autocomplete',
				header: 'Category',
				size: 200,
			},
			{
				accessorKey: 'priority',
				enableClickToCopy: true,
				filterVariant: 'autocomplete',
				header: 'Priority',
				size: 150,
			},
			{
				accessorKey: 'dueDate',
				enableClickToCopy: true,
				filterVariant: 'autocomplete',
				header: 'Due Date',
				size: 200,
			},
			{
				accessorKey: 'creationDate',
				enableClickToCopy: true,
				filterVariant: 'autocomplete',
				header: 'Creation Date',
				size: 200,
			},
		],
		[],
	);

	const { jwt, isAuthenticated } = useAuth();
	const [isAllRowsSelected, setIsAllRowsSelected] = useState(false);
	useEffect(() => {
		if (isAuthenticated) {
			fetchTasks(setTaskCallback, jwt);
		}
	}, []);

	const table = useMaterialReactTable({
		columns,
		data,
		enableColumnFilterModes: true,
		enableColumnOrdering: true,
		enableGrouping: true,
		enableColumnPinning: true,
		enableRowActions: true,
		enableRowSelection: true,
		initialState: {
			showColumnFilters: true,
			showGlobalFilter: true,
			columnPinning: {
				left: ['mrt-row-expand', 'mrt-row-select'],
				right: ['mrt-row-actions'],
			},
		},
		paginationDisplayMode: 'pages',
		positionToolbarAlertBanner: 'bottom',
		muiSearchTextFieldProps: {
			size: 'small',
			variant: 'outlined',
		},
		muiPaginationProps: {
			color: 'secondary',
			rowsPerPageOptions: [10, 20, 30],
			shape: 'rounded',
			variant: 'outlined',
		},
		renderTopToolbar: ({ table }) => {
			const handleDeleteTask = () => {
				deleteAllTasks(setTaskCallback, jwt);
				setIsAllRowsSelected(false);
			};
			return (
				<Box
					sx={(theme) => ({
						backgroundColor: lighten(
							theme.palette.background.default,
							0.05,
						),
						display: 'flex',
						gap: '0.5rem',
						p: '8px',
						justifyContent: 'space-between',
					})}
				>
					<Box
						sx={{
							display: 'flex',
							gap: '0.5rem',
							alignItems: 'center',
						}}
					>
						<MRT_GlobalFilterTextField table={table} />
						<MRT_ToggleFiltersButton table={table} />
					</Box>
					<Box>
						<Box sx={{ display: 'flex', gap: '0.5rem' }}>
							{isAllRowsSelected && (
								<Button
									color="secondary"
									disabled={!isAllRowsSelected}
									onClick={() => handleDeleteTask()}
									variant="contained"
								>
									Delete{' '}
									{table.getSelectedRowModel().rows.length}{' '}
									row(s)
								</Button>
							)}
						</Box>
					</Box>
				</Box>
			);
		},
		renderRowActionMenuItems: ({ closeMenu, row }) => [
			<MenuItem
				key={0}
				onClick={() => {
					// TODO: Update logic
					closeMenu();
				}}
			>
				Update Record
			</MenuItem>,
			<MenuItem
				key={1}
				onClick={() => {
					deleteTaskFromDB(setTaskCallback, row.original.id, jwt);
					closeMenu();
				}}
			>
				Delete Record
			</MenuItem>,
		],
	});

	useEffect(() => {
		setIsAllRowsSelected(table.getIsAllRowsSelected());
	}, [table.getIsAllRowsSelected()]);

	return <MaterialReactTable table={table} />;
};
