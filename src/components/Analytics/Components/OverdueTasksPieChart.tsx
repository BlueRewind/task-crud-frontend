import {
	PieChart,
	Pie,
	Tooltip,
	Legend,
	ResponsiveContainer,
	Cell,
} from 'recharts';
import { BoxComponent, StyledTypography } from '../../styled';
import { Task } from '../../../types';

interface OverdueAnalysisProps {
	tasks: Task[];
}

const COLOURS: string[] = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

export const OverdueAnalysis = ({
	tasks,
}: OverdueAnalysisProps): JSX.Element => {
	const calculateOverdueTasks = () => {
		const today = new Date();
		return tasks.filter((task) => new Date(task.dueDate) < today);
	};

	const countOverdueTasksByName = (): any => {
		const overdueTasks = calculateOverdueTasks();
		const counts: { [name: string]: number } = {};

		overdueTasks.forEach((task) => {
			counts[task.name] = (counts[task.name] || 0) + 1;
		});

		return Object.entries(counts).map(([name, count]) => ({ name, count }));
	};

	const overdueTasks = countOverdueTasksByName();
	return (
		<BoxComponent>
			<StyledTypography variant="h5">Overdue Tasks</StyledTypography>
			<ResponsiveContainer width="100%" height={300}>
				<PieChart>
					<Pie
						data={overdueTasks}
						dataKey="count"
						nameKey="name" // Assuming "name" should be used as the name key
						fill="#8884d8"
						label
					>
						{overdueTasks.map((_: any, index: number) => (
							<Cell
								key={`cell-${index}`}
								fill={COLOURS[index % COLOURS.length]}
							/>
						))}
					</Pie>
					<Tooltip />
					<Legend />
				</PieChart>
			</ResponsiveContainer>
		</BoxComponent>
	);
};
