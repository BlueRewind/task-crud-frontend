import { PieChart, Pie, Cell, ResponsiveContainer, Tooltip } from 'recharts';
import { Task } from '../../../types';
import { BoxComponent, StyledTypography } from '../../styled';

interface DistributionChartProps {
	tasks: Task[];
}

const COLOURS: string[] = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

export const TaskDistributionChart = ({
	tasks,
}: DistributionChartProps): JSX.Element => {
	const calculateTaskDistribution = (taskArr: Task[]) => {
		const distributionMap = {} as any;
		taskArr.forEach((task: Task) => {
			const priority = task.priority;
			distributionMap[priority] = (distributionMap[priority] || 0) + 1;
		});

		return Object.keys(distributionMap).map((key) => ({
			name: key,
			value: distributionMap[key],
		}));
	};
	const taskDistribution = calculateTaskDistribution(tasks);

	return (
		<BoxComponent>
			<StyledTypography variant="h5">
				Distribution of Priority
			</StyledTypography>
			<ResponsiveContainer width="100%" height={200}>
				<PieChart>
					<Pie
						data={taskDistribution}
						dataKey="value"
						nameKey="name"
						cx="50%"
						cy="50%"
						outerRadius={80}
						fill="#8884d8"
						label
					>
						{taskDistribution.map((_, index) => (
							<Cell
								key={`cell-${index}`}
								fill={COLOURS[index % COLOURS.length]}
							/>
						))}
					</Pie>
					<Tooltip />
				</PieChart>
			</ResponsiveContainer>
		</BoxComponent>
	);
};

export default TaskDistributionChart;
