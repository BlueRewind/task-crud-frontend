import * as React from 'react';
import { cleanup, render } from '@testing-library/react';
import { describe, it, expect, vi, beforeAll, afterAll } from 'vitest';

import { TaskCategoryDistribution } from '../CategoryDistribution';

import { Task, TaskCategory, TaskStatus } from '../../../../types';
import * as theme from '@emotion/react';
import { ThemeProvider } from '../../../../ThemeContext';
import { lightTheme } from '../../../../themes';
import { TaskCreationTrend } from '../CreationTrends';
import { OverdueAnalysis } from '../OverdueTasksPieChart';
import { RecentTasks } from '../RecentTasks';

const tasks: Task[] = [
	{
		id: '1',
		name: 'test-task',
		status: TaskStatus.CREATED,
		category: TaskCategory.EDUCATION,
		priority: '1',
		dueDate: new Date().toString(),
		creationDate: new Date().toString(),
	},
];

vi.spyOn(theme, 'useTheme').mockImplementation(() => lightTheme);

beforeAll(() => {
	global.ResizeObserver = class ResizeObserver {
		observe() {}
		unobserve() {}
		disconnect() {}
	};
});

afterAll(() => {
	cleanup();
});

const renderUnit = (child: React.ReactNode) => {
	return render(<ThemeProvider theme={lightTheme}>{child}</ThemeProvider>);
};

describe('Category Distribution', () => {
	it('should render the bar chart', () => {
		const { asFragment } = renderUnit(
			<TaskCategoryDistribution tasks={tasks} />,
		);
		expect(asFragment()).toMatchSnapshot();
	});

	it('should handle empty data gracefully', () => {
		const { getByText } = render(<TaskCategoryDistribution tasks={[]} />);
		expect(getByText('No data available')).toBeTruthy();
	});
});

describe('Creation Trend', () => {
	it('should render the bar chart', () => {
		const { asFragment } = renderUnit(<TaskCreationTrend tasks={tasks} />);
		expect(asFragment()).toMatchSnapshot();
	});

	it('should handle empty data gracefully', () => {
		const { getByText } = render(<TaskCreationTrend tasks={[]} />);
		expect(getByText('No data available')).toBeTruthy();
	});
});

describe('Overdue Analysis', () => {
	it('should render the bar chart', () => {
		const { asFragment } = renderUnit(<OverdueAnalysis tasks={tasks} />);
		expect(asFragment()).toMatchSnapshot();
	});

	it('should handle empty data gracefully', () => {
		const { getByText } = render(<TaskCreationTrend tasks={[]} />);
		expect(getByText('No data available')).toBeTruthy();
	});
});
