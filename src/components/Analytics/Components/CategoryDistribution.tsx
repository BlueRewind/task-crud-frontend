import { Typography } from '@mui/material';
import {
	BarChart,
	Bar,
	Tooltip,
	ResponsiveContainer,
	Cell,
	XAxis,
	YAxis,
} from 'recharts';
import { BoxComponent, StyledTypography } from '../../styled';
import { Task } from '../../../types';
import { useTheme } from '@emotion/react';

interface DistributionData {
	category: string;
	count: number;
}

interface TaskCategoryDistributionProps {
	tasks: Task[];
}

const COLOURS: string[] = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

export const TaskCategoryDistribution = ({
	tasks,
}: TaskCategoryDistributionProps): JSX.Element => {
	const theme: any = useTheme();
	const getCategoryDistributionData = (tasks: Task[]): DistributionData[] => {
		const taskCountsByCategory: { [category: string]: number } = {};
		tasks.forEach((task) => {
			if (task.category) {
				taskCountsByCategory[task.category] =
					(taskCountsByCategory[task.category] || 0) + 1;
			}
		});
		const categoryDistributionData: DistributionData[] = [];
		for (const category in taskCountsByCategory) {
			categoryDistributionData.push({
				category,
				count: taskCountsByCategory[category],
			});
		}

		return categoryDistributionData;
	};

	const data = getCategoryDistributionData(tasks);

	return (
		<BoxComponent data-testid="category-distribution">
			<StyledTypography variant="h5">
				Distribution of tasks
			</StyledTypography>
			<ResponsiveContainer width="95%" height={300}>
				{data.length === 0 ? (
					<Typography variant="h6">No data available</Typography>
				) : (
					<BarChart
						data={data}
						margin={{ top: 20, right: 30, left: 20, bottom: 50 }}
					>
						<XAxis
							dataKey="category"
							angle={-45}
							textAnchor="end"
							tick={{ fill: theme.palette.text.primary }}
						/>
						<YAxis
							allowDecimals={false}
							tick={{ fill: theme.palette.text.primary }}
						/>
						<Tooltip />
						<Bar dataKey="count" fill="#8884d8">
							{data.map((_, index) => (
								<Cell
									key={`cell-${index}`}
									fill={COLOURS[index % COLOURS.length]}
								/>
							))}
						</Bar>
					</BarChart>
				)}
			</ResponsiveContainer>
		</BoxComponent>
	);
};
