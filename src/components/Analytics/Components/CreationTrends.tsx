import {
	LineChart,
	Line,
	XAxis,
	YAxis,
	Tooltip,
	ResponsiveContainer,
} from 'recharts';
import { BoxComponent, StyledTypography } from '../../styled';
import { Task } from '../../../types';
import { useTheme } from '@emotion/react';

interface BarChartData {
	date: string;
	numOfTasks: number;
}

interface TaskCreationTrendProps {
	tasks: Task[];
}

export const TaskCreationTrend = ({
	tasks,
}: TaskCreationTrendProps): JSX.Element => {
	const theme: any = useTheme();
	const getTaskCreationTrendData = (tasks: Task[]): BarChartData[] => {
		const taskCountsByDate: { [date: string]: number } = {};

		tasks.forEach((task) => {
			const creationDate = task.creationDate.split('T')[0];
			taskCountsByDate[creationDate] =
				(taskCountsByDate[creationDate] || 0) + 1;
		});

		const taskCreationTrendData: { date: string; numOfTasks: number }[] =
			[];
		for (const date in taskCountsByDate) {
			taskCreationTrendData.push({
				date,
				numOfTasks: taskCountsByDate[date],
			});
		}

		return taskCreationTrendData;
	};

	return (
		<BoxComponent>
			<StyledTypography variant="h5">
				Tasks created overtime
			</StyledTypography>
			<ResponsiveContainer width="100%" height={300}>
				<LineChart
					data={getTaskCreationTrendData(tasks)}
					margin={{ top: 20, right: 30, left: 20, bottom: 5 }}
				>
					<XAxis
						dataKey="date"
						tick={{ fill: theme.palette.text.primary }}
					/>
					<YAxis tick={{ fill: theme.palette.text.primary }} />
					<Tooltip />
					<Line
						type="monotone"
						dataKey="numOfTasks"
						stroke="#8884d8"
					/>
				</LineChart>
			</ResponsiveContainer>
		</BoxComponent>
	);
};
