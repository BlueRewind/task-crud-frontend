import { Typography } from '@mui/material';
import { Task } from '../../../types';
import { BoxComponent, StyledTypography } from '../../styled';

interface RecentTasksProps {
	tasks: Task[];
}

export const RecentTasks = ({ tasks }: RecentTasksProps) => {
	return (
		<BoxComponent>
			<StyledTypography variant="h5">Recent Tasks</StyledTypography>
			<ul>
				{tasks.map((task: Task) => (
					<Typography variant="h5" key={task.id}>
						{task.name}
					</Typography>
				))}
			</ul>
		</BoxComponent>
	);
};
