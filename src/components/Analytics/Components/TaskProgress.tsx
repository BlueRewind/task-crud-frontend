import { Task, TaskStatus } from '../../../types';
import { BoxComponent, StyledTypography } from '../../styled';

interface TaskProgressProps {
	tasks: Task[];
}

export const TaskProgress = ({ tasks }: TaskProgressProps): JSX.Element => {
	const tasksCompleted: number = tasks.filter(
		(task) => task.status === TaskStatus.FINISHED,
	).length;

	return (
		<BoxComponent>
			<StyledTypography variant="h5">Tasks Completed</StyledTypography>
			<StyledTypography variant="h2">
				{tasksCompleted} / {tasks.length}
			</StyledTypography>
		</BoxComponent>
	);
};
