import * as React from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import TaskIcon from '@mui/icons-material/Assignment';
import DashboardIcon from '@mui/icons-material/Dashboard';
import {
	AppBar,
	DrawerHeader,
	Main,
	RightAlignedStack,
	StyledTypography,
} from './styled';
import { Button, Stack } from '@mui/material';
import { useAuth } from '../AuthContext';
import { useNavigate } from 'react-router-dom';
import { useThemeContext } from '../ThemeContext';

const drawerWidth = 240;

interface PersistentDrawerProps {
	children: React.ReactNode;
}

export default function PersistentDrawerLeft({
	children,
}: PersistentDrawerProps) {
	const { logout } = useAuth();
	const { theme, isDarkMode, toggleTheme } = useThemeContext();

	const navigate = useNavigate();

	const [open, setOpen] = React.useState(false);

	const handleDrawerOpen = () => {
		setOpen(true);
	};

	const handleDrawerClose = () => {
		setOpen(false);
	};

	return (
		<Box sx={{ display: 'flex' }}>
			<CssBaseline />
			<AppBar position="fixed" open={open}>
				<Toolbar>
					<IconButton
						color="inherit"
						aria-label="open drawer"
						onClick={handleDrawerOpen}
						edge="start"
						sx={{ mr: 2, ...(open && { display: 'none' }) }}
					>
						<MenuIcon />
					</IconButton>
					<Stack direction="row" spacing={1}>
						<StyledTypography variant="h4">Tasks</StyledTypography>
					</Stack>
					<RightAlignedStack direction="row" spacing={1}>
						<Button
							variant="contained"
							color="primary"
							sx={{ marginRight: '1vw' }}
							onClick={() => toggleTheme()}
						>
							{isDarkMode ? 'Light Theme' : 'Dark Theme'}
						</Button>
						<Button
							variant="contained"
							color="info"
							sx={{ marginRight: '1vw' }}
						>
							API Status
						</Button>
						<Button
							variant="contained"
							color="secondary"
							onClick={logout}
						>
							Sign out
						</Button>
					</RightAlignedStack>
				</Toolbar>
			</AppBar>
			<Drawer
				sx={{
					width: drawerWidth,
					flexShrink: 0,
					'& .MuiDrawer-paper': {
						width: drawerWidth,
						boxSizing: 'border-box',
					},
				}}
				variant="persistent"
				anchor="left"
				open={open}
			>
				<DrawerHeader>
					<IconButton onClick={handleDrawerClose}>
						{theme.direction === 'ltr' ? (
							<ChevronLeftIcon />
						) : (
							<ChevronRightIcon />
						)}
					</IconButton>
				</DrawerHeader>
				<Divider />
				<List>
					<ListItem
						key="Analytics"
						disablePadding
						onClick={() => navigate('/dashboard')}
					>
						<ListItemButton>
							<ListItemIcon>
								<DashboardIcon />
							</ListItemIcon>
							<ListItemText primary="Analytics" />
						</ListItemButton>
					</ListItem>
					<ListItem
						key="Tasks"
						disablePadding
						onClick={() => navigate('/tasks')}
					>
						<ListItemButton>
							<ListItemIcon>
								<TaskIcon />
							</ListItemIcon>
							<ListItemText primary="Tasks" />
						</ListItemButton>
					</ListItem>
				</List>
			</Drawer>
			<Main open={open}>
				<DrawerHeader />
				{children}
			</Main>
		</Box>
	);
}
