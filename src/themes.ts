import { createTheme } from '@mui/material';

export const lightTheme = createTheme({
	palette: {
		mode: 'light',
		background: {
			default: '#fafafa',
			paper: '#fff',
		},
		text: {
			primary: '#000',
			secondary: 'rgba(0, 0, 0, 0.7)',
		},
		primary: {
			main: '#3f51b5',
		},
		secondary: {
			main: '#f50057',
		},
		error: {
			main: '#d32f2f',
		},
	},
});

export const darkTheme = createTheme({
	palette: {
		mode: 'dark',
		background: {
			default: '#252525',
			paper: '#424242',
		},
		text: {
			primary: '#fff',
			secondary: 'rgba(255, 255, 255, 0.7)',
		},
		primary: {
			main: '#3f51b5',
		},
		secondary: {
			main: '#f50057',
		},
	},
});
