import Keycloak, { KeycloakInstance } from 'keycloak-js';
import { createContext, useContext, useEffect, useState } from 'react';

interface KeycloakContextType {
    isAuthenticated: () => boolean;
    isTokenExpired: () => boolean;
    getKeycloakUserInfo: () => Promise<any>;
    grabToken: () => any,
    login: () => void;
    logout: () => void;
}

const KeycloakContext = createContext<KeycloakContextType>({
    isAuthenticated: () => false,
    isTokenExpired: () => true,
    getKeycloakUserInfo: () => Promise.resolve(null),
    grabToken: () => '',
    login: () => {},
    logout: () => {},
});

interface KeycloakProviderProps {
    children: React.ReactNode;
}

const KeycloakProvider = ({ children }: KeycloakProviderProps) => {
    const initOptions = {
        url: 'http://localhost:8080/',
        realm: 'my-realm',
        clientId: 'task-app',
    };

    const [keycloak, setKeycloak] = useState<KeycloakInstance | null>(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const keycloakInstance = new Keycloak(initOptions);
        keycloakInstance
            .init({ onLoad: 'login-required', checkLoginIframe: true, pkceMethod: 'S256' })
            .then((auth) => {
                if (!auth) {
                    window.location.reload();
                } else {
                    setKeycloak(keycloakInstance);
                    setLoading(false);
                }
                setTimeout(() => {
                    keycloakInstance.updateToken(70).catch((e) => {
                        console.error('Failed to refresh token');
                    });
                }, 60000);
            })
            .catch(() => {
                console.error('Authenticated Failed');
            });
    }, []);

    const isAuthenticated = () => {
        return keycloak?.authenticated ?? false;
    };

    const grabToken = () => {
        return keycloak?.token ?? 'not a token';
    }

    const login = () => {
        keycloak?.login();
    };

    const logout = () => {
        keycloak?.logout();
    };

    const getKeycloakUserInfo = async () => {
        if (!keycloak) return null;
        const data = await keycloak.loadUserInfo();
        return data;
    };

    const isTokenExpired = () => {
        return keycloak?.isTokenExpired() ?? true;
    };

    if (loading) return <div>Loading...</div>;

    return (
        <KeycloakContext.Provider
            value={{
                isAuthenticated,
                isTokenExpired,
                getKeycloakUserInfo,
                login,
                logout,
                grabToken
            }}
        >
            {children}
        </KeycloakContext.Provider>
    );
};

const useKeycloak = () => {
    return useContext(KeycloakContext);
};

export { KeycloakProvider, useKeycloak };
