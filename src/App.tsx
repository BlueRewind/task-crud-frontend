import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { AuthProvider } from './AuthContext';
import LoginPage from './pages/Login';
import { Tasks } from './pages/Tasks';
import { CssBaseline } from '@mui/material';
import { AppContainer } from './components/styled';
import { Analytics } from './pages/Analytics';
import '../global.css';
import { ThemeProvider } from './ThemeContext';
import { darkTheme } from './themes';

function App() {
	return (
		<AppContainer>
			<ThemeProvider theme={darkTheme}>
				<CssBaseline>
					<BrowserRouter>
						<AuthProvider>
							<Routes>
								<Route path="/" element={<LoginPage />} />
								<Route path="/tasks" element={<Tasks />} />
								<Route
									path="/dashboard"
									element={<Analytics />}
								/>
							</Routes>
						</AuthProvider>
					</BrowserRouter>
				</CssBaseline>
			</ThemeProvider>
		</AppContainer>
	);
}

export default App;
