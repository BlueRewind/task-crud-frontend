import { useEffect, useState } from 'react';
import { Task } from '../types';
import { TaskCategoryDistribution } from '../components/Analytics/Components/CategoryDistribution';
import { TaskCreationTrend } from '../components/Analytics/Components/CreationTrends';
import { OverdueAnalysis } from '../components/Analytics/Components/OverdueTasksPieChart';
import { RecentTasks } from '../components/Analytics/Components/RecentTasks';
import TaskDistributionChart from '../components/Analytics/Components/PriorityPieChart';
import { TaskProgress } from '../components/Analytics/Components/TaskProgress';
import { StyledTypography } from '../components/styled';
import { Box, Grid } from '@mui/material';
import { useAuth } from '../AuthContext';
import { fetchTasks } from '../utils/api';
import PersistentDrawerLeft from '../components/AppDrawer';

export const Analytics = (): JSX.Element => {
	const [tasks, setTasks] = useState([] as Task[]);
	const { jwt } = useAuth();

	useEffect(() => {
		fetchTasks(setTasks, jwt);
	}, [jwt]);

	return (
		<Box>
			<PersistentDrawerLeft>
				<StyledTypography variant="h2">Dashboard</StyledTypography>
				<Grid container spacing={4}>
					<Grid item xs={12} sm={6} md={4}>
						<OverdueAnalysis tasks={tasks} />
					</Grid>
					<Grid item xs={12} sm={6} md={4}>
						<TaskCreationTrend tasks={tasks} />
					</Grid>
					<Grid item xs={12} sm={6} md={4}>
						<TaskCategoryDistribution tasks={tasks} />
					</Grid>
					<Grid item xs={12} sm={6} md={4}>
						<TaskProgress tasks={tasks} />
					</Grid>
					<Grid item xs={12} sm={6} md={4}>
						<RecentTasks tasks={tasks.slice(0, 3).reverse()} />
					</Grid>
					<Grid item xs={12} sm={6} md={4}>
						<TaskDistributionChart tasks={tasks} />
					</Grid>
				</Grid>
			</PersistentDrawerLeft>
		</Box>
	);
};
