import {
	Button,
	Card,
	CardContent,
	TextField,
	Typography,
} from '@mui/material';
import { useState } from 'react';
import { useAuth } from '../AuthContext';

const Login = () => {
	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');
	const [error, setError] = useState('');

	const { login } = useAuth();

	const handleLogin = async () => {
		if (!username || !password) {
			setError('Please enter both username and password.');
			return;
		}

		try {
			await login(username, password);
		} catch (error) {
			console.error('Authentication failed:', error);
			setError('Authentication failed. Please check your credentials.');
		}
	};

	return (
		<div
			style={{
				display: 'flex',
				justifyContent: 'center',
				alignItems: 'center',
				height: '100vh',
			}}
		>
			<Card>
				<CardContent>
					<Typography variant="h5" component="div" gutterBottom>
						Sign in
					</Typography>
					<div>
						<TextField
							label="Username"
							variant="outlined"
							value={username}
							onChange={(e) => setUsername(e.target.value)}
							fullWidth
							margin="normal"
						/>
					</div>
					<div>
						<TextField
							label="Password"
							type="password"
							variant="outlined"
							value={password}
							onChange={(e) => setPassword(e.target.value)}
							fullWidth
							margin="normal"
						/>
					</div>
					<Button variant="contained" onClick={handleLogin}>
						Sign in
					</Button>
					{error && (
						<Typography
							variant="body2"
							style={{ color: 'red', marginTop: '8px' }}
						>
							{error}
						</Typography>
					)}
				</CardContent>
			</Card>
		</div>
	);
};

export default Login;
