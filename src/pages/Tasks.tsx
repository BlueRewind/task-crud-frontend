import { Box, Button, Grid } from '@mui/material';
import { useEffect, useState } from 'react';
import { Task } from '../types';
import TaskPopup from '../components/TaskPopup';
import { TasksTable } from '../components/TasksTable';
import { AppContainer } from '../components/styled';
import { fetchTasks } from '../utils/api';
import { useAuth } from '../AuthContext';
import AddIcon from '@mui/icons-material/Add';
import PersistentDrawerLeft from '../components/AppDrawer';

export const Tasks = (): JSX.Element => {
	const [popupOpen, setPopupOpen] = useState(false);
	const [tasks, setTasks] = useState([] as Task[]);
	const { jwt } = useAuth();

	const handleCreateTask = () => {
		setPopupOpen(true);
	};

	const handleClosePopup = () => {
		setPopupOpen(false);
	};

	const handleSetTasks = (updatedTasks: Task[]) => {
		setTasks(updatedTasks);
	};

	useEffect(() => {
		fetchTasks(setTasks, jwt);
	}, []);

	return (
		<Box sx={{ display: 'flex' }}>
			<AppContainer>
				<PersistentDrawerLeft>
					<Box mt={2} />
					<Grid container spacing={2}>
						<Grid item xs={12}>
							<Button
								variant="contained"
								color="success"
								onClick={handleCreateTask}
								endIcon={<AddIcon />}
							>
								CREATE
							</Button>
						</Grid>
					</Grid>
					<Box mt={2} />
					<TaskPopup
						open={popupOpen}
						onClose={handleClosePopup}
						setTasksCallback={handleSetTasks}
					/>
					<TasksTable setTaskCallback={handleSetTasks} data={tasks} />
				</PersistentDrawerLeft>
			</AppContainer>
		</Box>
	);
};
