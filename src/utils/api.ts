import axios from 'axios';
import { Task } from '../types';

const url = '/api/tasks';
export const fetchTasks = async (setTasks: any, jwt: string) => {
	try {
		const response = await axios.get(url, {
			headers: {
				Accept: 'application/json, text/plain, */*',
				'Access-Control-Allow-Origin': '*',
				Authorization: 'Bearer ' + jwt,
			},
		});
		const tasks = response.data;
		setTasks(tasks);
	} catch (error) {
		console.error('Error fetching data:', error);
	}
};

export const deleteAllTasks = async (setTasks: any, jwt: string) => {
	try {
		await axios.delete(url, {
			headers: {
				Accept: 'application/json, text/plain, */*',
				'Access-Control-Allow-Origin': '*',
				Authorization: 'Bearer ' + jwt,
			},
		});
		setTasks([] as Task[]);
	} catch (error) {
		console.error('Error when deleting tasks: ', error);
	}
};
