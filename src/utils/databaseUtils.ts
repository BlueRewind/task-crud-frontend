import axios from 'axios';
import { Task } from '../types';
import { fetchTasks } from './api';

export const deleteTaskFromDB = async (
	setTasks: (updatedTasks: Task[]) => void,
	uuid: string,
	jwt: string,
) => {
	try {
		const url = `/api/tasks/${uuid}`;
		await axios.delete(url, {
			headers: {
				Accept: 'application/json, text/plain, */*',
				'Access-Control-Allow-Origin': '*',
				Authorization: 'Bearer ' + jwt,
			},
		});
		fetchTasks(setTasks, jwt);
	} catch (error) {
		console.error('Error fetching data:', error);
	}
};
