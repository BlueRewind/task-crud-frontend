import { createContext, useContext, useState } from 'react';
import { Theme, ThemeProvider as MuiThemeProvider } from '@mui/material/styles';
import { lightTheme, darkTheme } from './themes';

interface ThemeContextType {
	theme: Theme;
	isDarkMode: boolean;
	toggleTheme: () => void;
}

const ThemeContext = createContext<ThemeContextType | undefined>(undefined);

export const useThemeContext = () => {
	const context = useContext(ThemeContext);
	if (!context) {
		throw new Error('useThemeContext must be used within a ThemeProvider');
	}
	return context;
};

interface ThemeProviderProps {
	children: React.ReactNode;
}

export const ThemeProvider = ({ children }: any): JSX.Element => {
	const [isDarkMode, setIsDarkMode] = useState<boolean>(false);

	const toggleTheme = () => {
		setIsDarkMode((prevMode) => !prevMode);
	};

	const theme = isDarkMode ? darkTheme : lightTheme;

	return (
		<MuiThemeProvider theme={theme}>
			<ThemeContext.Provider value={{ theme, isDarkMode, toggleTheme }}>
				{children}
			</ThemeContext.Provider>
		</MuiThemeProvider>
	);
};
